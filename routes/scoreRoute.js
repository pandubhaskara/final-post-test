const express = require('express')
const router = express.Router()
const score = require('../controllers/score')

router.get('/', score.getScore)
router.get('/:id', score.getScoreByStudentId)
router.post('/', score.postScore)
router.put('/:id', score.updateScore)
router.delete('/:id', score.deleteScore)

module.exports = router