const express = require('express')
const router = express.Router()
const students = require('../controllers/students')

router.get('/', students.getStudent)
router.get('/:id', students.getStudentById)
router.post('/', students.postStudent)
router.put('/:id', students.updateStudent)
router.delete('/:id', students.deleteStudent)

module.exports = router