const express=require('express')
const router=express.Router()
const admin= require('./adminRoute')
const students= require('./studentsRoute')
const score= require('./scoreRoute')
const auth= require('../middleware/auth')
const {authentication} = require('../middleware/auth');

router.use('/', admin)
router.use('/students', authentication, students)
router.use('/scores', authentication, score)

module.exports=router
