const Joi = require("joi");
const students= require('../models').students;
const score = require("../models").score;

module.exports = {
  postStudent: async (req, res) => {
    const body = req.body;
    try {
      const schema = Joi.object({
        name: Joi.string().required(),
        dateOfBirth: Joi.string().required(),
        address: Joi.string().required(),
      });

      const { error } = schema.validate(
        {
          name: body.name,
          dateOfBirth: body.dateOfBirth,
          address: body.address,
        },
        { abortEarly: false }
      );

      if (error) {
        return res.status(400).json({
          status: "failed",
          message: "Bad Request",
          errors: error["details"][0]["message"],
        });
      }

      const check = await students.create({
        name: body.name,
        dateOfBirth: body.dateOfBirth,
        address: body.address,
      });

      if (!check) {
        return res.status(400).json({
          status: "failed",
          message: "Unable to save the data to database",
        });
      }
      return res.status(200).json({
        status: "success",
        message: "Successfully saved to database",
        data: check,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        status: "failed",
        message: "Internal Server Error",
      });
    }
  },
  getStudent: async (req, res) => {
    try {
      const data = await students
      .findAll({
        include: [{
          model: score,
          as: 'score'
        }],
        order: [
          ['id', 'ASC'],
          [{ model: score, as: 'score' }, 'createdAt', 'DESC'],
        ],
      });
      if (!data) {
        return res.status(404).json({
          status: "failed",
          message: "Data not found",
          data: [],
        });
      }
      return res.status(200).json({
        status: "success",
        message: "Successfully retrieved students tables",
        data: data,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        status: "failed",
        message: "Internal Server Error",
      });
    }
  },
  getStudentById:async(req,res)=>{
    try{
      const data = await students.findByPk(req.params.id, 
        {
          include: [{
            model: score,
            as: 'score'
          }],
      });
      if(!data){
        res.status(404).json({
          status: "failed",
          message: "Data not found",
          data: []
        });
      }else{
        res.status(200).json({
          status: "success",
          message: "Successfully retrieved students tables",
          data: data
        }) 
      }
    }catch(error){
      res.status(400).json({
        status: 'error',
        message: "internal server error"
      })
    }
  },
  updateStudent: async (req, res) => {
    const body = req.body;
    try {
      const schema = Joi.object({
        name: Joi.string(),
        dateOfBirth: Joi.string(),
        address: Joi.string(),
      });

      const { error } = schema.validate(
        {
          name: body.name,
          dateOfBirth: body.dateOfBirth,
          address: body.address,
        },
        { abortEarly: false }
      );

      if (error) {
        return res.status(400).json({
          status: "failed",
          message: "Bad Request",
          errors: error["details"][0]["message"],
        });
      }

      const updatedStudent = await students.update(
        { ...body },
        {
          where: {
            id: req.params.id,
          },
        }
      );

      if (!updatedStudent[0]) {
        return res.status(400).json({
          status: "failed",
          message: "Unable to update database",
        });
      }

      const data = await students.findOne({
        where: {
          id: req.params.id,
        },
      });

      return res.status(200).json({
        status: "success",
        message: "Data updated successfully",
        data: data,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        status: "failed",
        message: "Internal Server Error",
      });
    }
  },
  deleteStudent: async (req, res) => {
    const id = req.params.id;
    try {
      const check = await students.destroy({
        where: {
          id, // id : id
        },
      });
      if (!check) {
        return res.status(400).json({
          status: "failed",
          message: "Unable to delete the data",
        });
      }
      return res.status(200).json({
        status: "success",
        message: "Deleted successfully",
      });
    } catch (error) {}
  },
};
