const Joi = require("joi");
const {score} = require("../models");



module.exports = {
  postScore: async (req, res) => {
    const body = req.body;
    try {
      const schema = Joi.object({
        studentsId: Joi.number().required(),
        math: Joi.number().required(),
        physics: Joi.number().required(),
        algorithm: Joi.number().required(),
        programming: Joi.number().required(),
      });

      const { error } = schema.validate(
        {
          studentsId: body.studentsId,
          math: body.math,
          physics: body.physics,
          algorithm: body.algorithm,
          programming: body.programming,
        },
        { abortEarly: false }
      );

      if (error) {
        return res.status(400).json({
          status: "failed",
          message: "Bad Request",
          errors: error["details"][0]["message"],
        });
      }

      const check = await score.create({
        studentsId: body.studentsId,
        math: body.math,
        physics: body.physics,
        algorithm: body.algorithm,
        programming: body.programming,
      });

      if (!check) {
        return res.status(400).json({
          status: "failed",
          message: "Unable to save the data to database",
        });
      }
      return res.status(200).json({
        status: "success",
        message: "Successfully saved to database",
        data: check,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        status: "failed",
        message: "Internal Server Error",
      });
    }
  },
  getScore: async (req, res) => {
    try {
      const scores = await score.findAll();
      if (!scores) {
        return res.status(404).json({
          status: "failed",
          message: "Data not found",
          data: [],
        });
      }
      return res.status(200).json({
        status: "success",
        message: "Successfully retrieved scores table",
        data: scores,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        status: "failed",
        message: "Internal Server Error",
      });
    }
  },
  getScoreByStudentId:async(req,res)=>{
    try{
      const data = await score.findOne({ where: {studentsId: req.params.id}});
      if(!data){
        res.status(404).json({
          status: "failed",
          message: "Data not found",
          data: []
        });
      }else{
        res.status(200).json({
          status: "success",
          message: "Successfully retrieved score tables",
          data: data
        }) 
      }
    }catch(error){
      res.status(400).json({
        status: 'error',
        message: "internal server error"
      })
    }
  },
  updateScore: async (req, res) => {
    const body = req.body;
    try {
      const schema = Joi.object({
        studentsId: Joi.number(),
        math: Joi.number(),
        physics: Joi.number(),
        algorithm: Joi.number(),
        programming: Joi.number(),
      });

      const { error } = schema.validate(
        {
            // studentsId: body.studentsId,
            math: body.math,
            physics: body.physics,
            algorithm: body.algorithm,
            programming: body.programming,
        },
        { abortEarly: false }
      );

      if (error) {
        return res.status(400).json({
          status: "failed",
          message: "Bad Request",
          errors: error["details"][0]["message"],
        });
      }

      const updatedScore = await score.update(
        { ...body },
        {
          where: {
            studentsId: req.params.id,
          },
        }
      );

      if (!updatedScore[0]) {
        return res.status(400).json({
          status: "failed",
          message: "Unable to update database",
        });
      }

      const data = await score.findOne({
        where: {
          studentsId: req.params.id,
        },
      });

      return res.status(200).json({
        status: "success",
        message: "Data updated successfully",
        data: data,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        status: "failed",
        message: "Internal Server Error",
      });
    }
  },
  deleteScore: async (req, res) => {
    const studentsId = req.params.studentsId;
    try {
      const check = await score.destroy({
        where: {
          studentsId: req.params.id, // id : id
        },
      });
      if (!check) {
        return res.status(400).json({
          status: "failed",
          message: "Unable to delete the data",
        });
      }
      return res.status(200).json({
        status: "success",
        message: "Deleted successfully",
      });
    } catch (error) {}
  },
};
