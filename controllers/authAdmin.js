const Joi = require("joi");
const admin = require("../models").admin;
const bcrypt = require('bcrypt');
const jwt =require('jsonwebtoken')
require('dotenv').config()

module.exports = {
  register: async (req, res) => {
    const body = req.body;
    try {
      const schema = Joi.object({
        username: Joi.string().required(),
        password: Joi.string().required(),
      });

      const { error } = await schema.validate(
        {
          username: body.username,
          password: body.password,
        },
        { abortEarly: false }
      );
      //   return res.status(200).send(check)

      if (error) {
        return res.status(400).json({
          status: "failed",
          message: "Bad Request",
          errors: error["details"][0]["message"],
        });
      }
      const salt = await bcrypt.genSalt(10)
      bcrypt.hash(body.password, salt, function (err, result) {
        return admin.create({
          username: body.username,
          password: result
        });
      })
      
      return res.status(200).json({
        status: "success",
        message: "Successfully saved to database"
      });
    } catch (error) {
      console.log(error)
        return res.status(500).json({
            status:"failed",
            message: "internal server error"
        });
    }
  },
  login: async (req, res) => {
    const body = req.body;
    try {
      const schema = Joi.object({
        username: Joi.string().required(),
        password: Joi.string().required(),
      });
      const { error } = schema.validate({ ...body });
      if (error) {
        return res.status(400).json({
          status: "failed",
          message: error.message,
        });
      }
      const user = await admin.findOne({where: { username : body.username}});
      if(user){
        const validPassword = await bcrypt.compare(body.password, user.password);
        if(validPassword){
          const username=body.username
          const user={ name: username}

          const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET)
          res.status(200).send({
            status :" Success",
            message : 'Successfully Login',
            token : accessToken
          })
        }else {
          res.status(400).json({error : 'invalid password'})        
        }
      }
      
      else{
        res.status(404).json({ error: 'user doesnt exist'})
      }
    
    }catch (error) {
      return res.status(400).json({
        error:'error',
        message:'invalid input'
      });
    }
  },
};
