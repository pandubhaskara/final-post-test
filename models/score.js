'use strict';
const {Model} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class score extends Model {
    static associate(models) {
      score.belongsTo(models.students,{
        foreignKey: "studentsId",
        as: "student"
      })
      
    }
  };
  score.init({
    studentsId: DataTypes.INTEGER,
    math: DataTypes.INTEGER,
    physics: DataTypes.INTEGER,
    algorithm: DataTypes.INTEGER,
    programming: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'score',
  });
  return score;
};