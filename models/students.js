'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class students extends Model {
    static associate(models) {
      students.hasOne(models.score,{
        foreignKey:'studentsId',
        as: 'score'
      })
    }
  };
  students.init({
    name: DataTypes.STRING,
    dateOfBirth: DataTypes.STRING,
    address: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'students',
  });
  return students;
};